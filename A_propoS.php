<!DOCTYPE html>
<html lang="fr">
<?php
include_once 'header.php';
?>

<body>
    <div class="container" style=" border-left: solid 2px rgb(132, 211, 213);border-right:solid 2px rgb(132, 211, 213);">
        <div class="container" id="bbg">
            <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
                <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
                    <img src="files/img/Gtr.png" alt="logo" width="32px" height="32px">
                    <span class="fs-4">roupe-4</span>
                </a>
                <ul class="nav nav-pills">
                    <li class="nav-item"><a href="./index.php" class="nav-link" aria-current="page" id="conv">Accueil</a></li>
                    <li class="nav-item"><a href="./index.php#trouver" class="nav-link" id="conv">Concept</a></li>
                    <li class="nav-item act"><a href="./A_propoS.php" class="nav-link" id="conv">A propos de Groupe-4</a></li>
                </ul>
            </header>
        </div>

        <p class="lead" style="text-align:center;">Une équipe de jeune dynamique passionnée d'informatique</p>

        <div style="text-align:center;margin-top:20px;margin-bottom:20px;">
            <h5 id="ta">EQUIPE GOUPE-4</h5>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card mb-3" style="max-width: 540px;">
                        <div class="row g-0">
                            <div class="col-md-4">
                                <img src="files/img/AKED.png" class="img-fluid rounded-start" alt="AKOWE Emmanuel">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">AKOWE Emmanuel Darius .S</h5>
                                    <p class="card-text">Developpement Front-End</p>
                                    <p class="card-text"><small class="text-muted"><a href="#">Contacter</a></small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card mb-3" style="max-width: 540px;">
                        <div class="row g-0">
                            <div class="col-md-4">
                                <img src="files/img/DTM.jpeg" class="img-fluid rounded-start" alt="DJOLOLO Tobi">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">DJOLOLO Tobi</h5>
                                    <p class="card-text">Developpement Back-end</p>
                                    <p class="card-text"><small class="text-muted"><a href="#">Contacter</a></small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card mb-3" style="max-width: 540px;">
                        <div class="row g-0">
                            <div class="col-md-4">
                                <img src="files/img/DG.png" class="img-fluid rounded-start" alt="GOSSOU Didier">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">GOSSOU Didier</h5>
                                    <p class="card-text">Developpement Web</p>
                                    <p class="card-text"><small class="text-muted"><a href="#">Contacter</a></small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card mb-3" style="max-width: 540px;">
                        <div class="row g-0">
                            <div class="col-md-4">
                                <img src="files/img/KPM.png" class="img-fluid rounded-start" alt="KPODEKON Maurille">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">KPODEKON Maurille</h5>
                                    <p class="card-text">Developpement Web</p>
                                    <p class="card-text"><small class="text-muted"><a href="#">Contacter</a></small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    include_once 'header.php';
    ?>
</body>

</html>